define(['custom'], function(custom) {

	
	var fileName  = 'home';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $timeout, cfpLoadingBar, $firebase) {	   
				    
					// INIT
					$scope.init = function(){
						custom.parallaxStart();	   
						cfpLoadingBar.start();
					};	
					
				    // fake the initial load so first time users can see it right away:
				    $timeout(function() {
				      cfpLoadingBar.complete();
				    }, 750);
				   
					$scope.lorem = "Lorem ipsum dolor sit amet, an epicuri mediocrem vituperata eos. Illud volumus periculis per no, cu torquatos definitionem eum. Est cu hendrerit vituperatoribus. In officiis nominati eum, aperiri dolorem usu ut. In meliore denique suavitate vim.";
					
				
	    
				});				
	    },
	    ///////////////////////////////////////
  };
});
